// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class GAMPRG2MidtermsEditorTarget : TargetRules
{
	public GAMPRG2MidtermsEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "GAMPRG2Midterms" } );
	}
}
