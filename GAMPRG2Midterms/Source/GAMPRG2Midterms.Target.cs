// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class GAMPRG2MidtermsTarget : TargetRules
{
	public GAMPRG2MidtermsTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "GAMPRG2Midterms" } );
	}
}
