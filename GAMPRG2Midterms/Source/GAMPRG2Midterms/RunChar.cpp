// Fill out your copyright notice in the Description page of Project Settings.

#include "RunChar.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "Engine/SkeletalMesh.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/FloatingPawnMovement.h"

// Sets default values
ARunChar::ARunChar()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMesh");
	// Set the static mesh as the root component
	SetRootComponent(SkeletalMesh);

	// Add floating pawn movement component
	Movement = CreateDefaultSubobject<UFloatingPawnMovement>("Movement");

	// Setup camera arm
	CameraArm = CreateDefaultSubobject<USpringArmComponent>("CameraArm");
	CameraArm->SetupAttachment(SkeletalMesh);
	// Initial length (can be modified in the blueprint)
	CameraArm->TargetArmLength = 500.0f;

	// Setup camera
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	// Attach the camera to the spring arm. This allows the camera to move with the spring arm
	Camera->SetupAttachment(CameraArm);

}

// Called when the game starts or when spawned
void ARunChar::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ARunChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARunChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}
