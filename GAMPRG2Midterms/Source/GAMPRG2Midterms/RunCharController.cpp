// Fill out your copyright notice in the Description page of Project Settings.

#include "RunCharController.h"
#include "Components/InputComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "RunChar.h"

ARunCharController::ARunCharController()
{

}

void ARunCharController::BeginPlay()
{
	Super::BeginPlay();
	runChar = Cast<ARunChar>(GetPawn());
}

void ARunCharController::MoveForward(float scale)
{
	const FRotator Rotation = runChar->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);


	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	runChar->AddMovementInput(Direction, scale);

}

void ARunCharController::MoveRight(float scale)
{
	const FRotator Rotation = runChar->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);


	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	runChar->AddMovementInput(Direction, scale);
}

void ARunCharController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	MoveForward(1.0);
	MoveRight(0);
}

void ARunCharController::SetupInputComponent()
{
	Super::SetupInputComponent();

	//check(InputComponent);
	InputComponent->BindAxis("MoveForward", this, &ARunCharController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ARunCharController::MoveRight);

}

