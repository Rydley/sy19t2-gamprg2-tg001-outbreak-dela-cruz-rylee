// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharController.generated.h"

/**
 * 
 */
UCLASS()
class GAMPRG2MIDTERMS_API ARunCharController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ARunCharController();

protected:
	virtual void BeginPlay() override;
	void MoveForward(float scale);
	void MoveRight(float scale);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupInputComponent() override;

private:
	class ARunChar* runChar;
};

