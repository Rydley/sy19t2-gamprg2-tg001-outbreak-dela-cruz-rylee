// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RunChar.generated.h"

UCLASS()
class GAMPRG2MIDTERMS_API ARunChar : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARunChar();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UFloatingPawnMovement* Movement;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USkeletalMeshComponent* SkeletalMesh;

	// Adding your own camera component allows you to override the default pawn camera. We want the camera to be behind of our pawn
	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UCameraComponent* Camera;

	// Camera can be attached to this so that when it detects collision it will adjust the camera's position.
	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USpringArmComponent* CameraArm;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};