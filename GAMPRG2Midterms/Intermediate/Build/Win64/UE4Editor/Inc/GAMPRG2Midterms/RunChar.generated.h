// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRG2MIDTERMS_RunChar_generated_h
#error "RunChar.generated.h already included, missing '#pragma once' in RunChar.h"
#endif
#define GAMPRG2MIDTERMS_RunChar_generated_h

#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_RPC_WRAPPERS
#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunChar(); \
	friend struct Z_Construct_UClass_ARunChar_Statics; \
public: \
	DECLARE_CLASS(ARunChar, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAMPRG2Midterms"), NO_API) \
	DECLARE_SERIALIZER(ARunChar)


#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_INCLASS \
private: \
	static void StaticRegisterNativesARunChar(); \
	friend struct Z_Construct_UClass_ARunChar_Statics; \
public: \
	DECLARE_CLASS(ARunChar, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAMPRG2Midterms"), NO_API) \
	DECLARE_SERIALIZER(ARunChar)


#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunChar(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunChar) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunChar); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunChar); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunChar(ARunChar&&); \
	NO_API ARunChar(const ARunChar&); \
public:


#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunChar(ARunChar&&); \
	NO_API ARunChar(const ARunChar&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunChar); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunChar); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunChar)


#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Movement() { return STRUCT_OFFSET(ARunChar, Movement); } \
	FORCEINLINE static uint32 __PPO__SkeletalMesh() { return STRUCT_OFFSET(ARunChar, SkeletalMesh); } \
	FORCEINLINE static uint32 __PPO__Camera() { return STRUCT_OFFSET(ARunChar, Camera); } \
	FORCEINLINE static uint32 __PPO__CameraArm() { return STRUCT_OFFSET(ARunChar, CameraArm); }


#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_9_PROLOG
#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_RPC_WRAPPERS \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_INCLASS \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_INCLASS_NO_PURE_DECLS \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPRG2Midterms_Source_GAMPRG2Midterms_RunChar_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
