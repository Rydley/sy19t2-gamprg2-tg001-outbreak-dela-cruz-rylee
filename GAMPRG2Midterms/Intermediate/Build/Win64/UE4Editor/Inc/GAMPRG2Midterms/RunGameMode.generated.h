// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRG2MIDTERMS_RunGameMode_generated_h
#error "RunGameMode.generated.h already included, missing '#pragma once' in RunGameMode.h"
#endif
#define GAMPRG2MIDTERMS_RunGameMode_generated_h

#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_RPC_WRAPPERS
#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunGameMode(); \
	friend struct Z_Construct_UClass_ARunGameMode_Statics; \
public: \
	DECLARE_CLASS(ARunGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/GAMPRG2Midterms"), NO_API) \
	DECLARE_SERIALIZER(ARunGameMode)


#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesARunGameMode(); \
	friend struct Z_Construct_UClass_ARunGameMode_Statics; \
public: \
	DECLARE_CLASS(ARunGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/GAMPRG2Midterms"), NO_API) \
	DECLARE_SERIALIZER(ARunGameMode)


#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunGameMode(ARunGameMode&&); \
	NO_API ARunGameMode(const ARunGameMode&); \
public:


#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunGameMode(ARunGameMode&&); \
	NO_API ARunGameMode(const ARunGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunGameMode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunGameMode)


#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_PRIVATE_PROPERTY_OFFSET
#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_12_PROLOG
#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_RPC_WRAPPERS \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_INCLASS \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_INCLASS_NO_PURE_DECLS \
	GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPRG2Midterms_Source_GAMPRG2Midterms_RunGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
